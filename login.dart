import 'package:flutter/material.dart';
import 'package:module_3_app/signUp.dart';

class login extends StatelessWidget {
  const login({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Login"),
      ),
      body: ElevatedButton(
          onPressed: () => {
                Navigator.push(
                    context, MaterialPageRoute(builder: (context) => signUp()))
              },
          child: const Text("SignUp")),
    );
  }
}
